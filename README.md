Previsione punteggio dei vini in base alle caratteristiche
==========================================================

*Progetto di Programmazione di Applicazioni Data Intensive 
a.a. 2018/19*

Realizzato da: Samuele Burattini

## Obiettivi del progetto
Obiettivo del progetto è addestrare un modello di regressione in grado di predire il punteggio del vino in base ad una descrizione fornita in linguaggio naturale e alcune caratteristiche su luogo e anno di produzione e prezzo.
Sono state impiegate tecniche di processamento del linguaggio naturale per estrarre le feature più rilevanti dalle descrizioni e diversi modelli di regressione per ottenere la performance migliore.

## Origine dei dati
Il dataset con le recensioni, punteggi e prezzi dei vini è stato ottenuto da [Kaggle](https://www.kaggle.com/zynicide/wine-reviews#winemag-data-130k-v2.csv).

Il dataset è stato ricavato scaricando le recensioni di circa 130000 vini postate sul sito [WineEnthusiast ](https://www.winemag.com/?s=&drink_type=wine) e inserendo le informazioni ricavate in formato csv. La rilevazione è stata effettuata nel 2017.
### Significato delle colonne
Le feature presenti nel Dataset originale sono: 

- `country`: paese in cui è stato prodotto il vino
- `description`: descrizione in linguaggio naturale del vino scritta da un sommelier del sito
- `designation`: designazione che viene attribuita al vino, è molto specifica e spesso identifica vini della stessa zona o con caratteristiche simili
- `price`: prezzo a bottiglia in dollari
- `province`: zona di produzione del vino è un'indicazione geografica più precisa rispetto allo stato (es. country:USA, province:California)
- `region_1`: una indicazione geografica ancora più specifica della zona di produzione che è ristretta all'interno della regione
- `region_2`: opzionale, spesso ridonda l'informazione indicata da region_1
- `taster_name`: il nome del sommelier che ha scritto la recensione
- `taster_twitter_handle`: l'alias twitter del sommelier che ha scritto la recensione
- `title`: titolo della recensione, contiene il nome del vino spesso compreso di anno di produzione
- `winery`: casa produttrice del vino

La variabile che tenteremo di predirre è:

- `points`: punteggio che è stato assegnato al vino da degli esperti, il punteggio è intero, in scala da 0 a 100, anche se sul sito vengono pubblicati solo vini con almeno 80 punti